#include "Integration.h"

double simpsonInt(Grid& grid)
{
	if (grid.x.size() % 2 == 0)
	{
		perror("Not odd number of nodes for Simpson integration!");
		exit(1);
	}

	size_t N = grid.n;

	double J = 0.0;

	// ����� �� ������ ��������
	J += grid.y[0];
	for (size_t i = 2; i < N - 1; i += 2)
	{
		J += 2 * grid.y[i];
	}
	J += grid.y[N - 1];

	// ����� �� �������� ��������
	for (size_t i = 1; i < N; i += 2)
	{
		J += 4 * grid.y[i];
	}

	J *= (grid.b - grid.a) / (grid.n - 1) / 3;

	return J;
}

double rectangleInt(Grid& grid)
{
	double J = 0.0;

	double h;

	// ���� ���� - ��������� ������
	if (grid.n == 1)
	{
		J += func((grid.a + grid.b) / 2);
		J *= grid.b - grid.a;
		return J;
	}
	else
	{
		h = (grid.b - grid.a) / (grid.n + 1);
	}

	// ������ �� ���������
	for (size_t i = 0; i < grid.n; i++)
	{
		J += func(grid.a + h / 2 + i * h);
	}

	J *= h;

	return J;
}

double trapeziumInt(Grid& grid)
{
	double J = 0.0;
	size_t N = grid.n;

	J += (grid.y[0] + grid.y[N - 1]) / 2.0;

	for (size_t i = 1; i < N - 1; i++)
	{
		J += grid.y[i];
	}

	J *= (grid.b - grid.a) / (N - 1);

	return J;
}

double computeSum(vector<double>& nodes, vector<double>& weights, double a, double b)
{
	double S = 0.0;

	// ���������� ����� �� [-1,1]
	for (size_t i = 0; i < nodes.size(); i++)
	{
		S += weights[i] * func(0.5 * (b - a) * nodes[i] + 0.5 * (a + b));
	}

	// �������� ����������� ������� [-1,1] �� [a,b]
	S *= (b - a) / 2.0;

	return S;
}

double legendre(double x, size_t n)
{
	// ������������ ����������
	double L_n0, L_n1, L_n2;

	L_n0 = 1;
	L_n1 = x;
	L_n2 = x; // �� ������, ���� n = 1

	// ������ ���������� � L_2(x) - ��������� ������ �������
	// �� ���������� ������� n
	// ������: ���� ����� ��-� ������� n=3, �� ����� ������� 2 ��������
	for (size_t i = 1; i <= n - 1; i++)
	{
		L_n2 = x * L_n1 * (2 * i + 1) / (i + 1) - L_n0 * i / (i + 1);

		// ���������� ���������� ��������
		L_n0 = L_n1;
		L_n1 = L_n2;
	}

	return L_n2;
}

double fork(double left, double right, double eps, size_t n)
{
	double middle, yMiddle, yLeft;

	middle = right;

	while (abs(right - left) > eps)
	{
		middle = (right + left) / 2;

		yMiddle = legendre(middle, n);
		yLeft = legendre(left, n);

		if (yMiddle == 0.0) return middle;

		if (yLeft * yMiddle < 0) right = middle;
		else left = middle;
	}

	return middle;
}

vector<double> findLegendreRoots(double eps, size_t n)
{
	double a = -1, b = 1;

	vector<double> roots;

	size_t nodesNumber = 10;
	double h = (b - a) / (nodesNumber - 1);

	vector<vector<double>> xSignChangeStore;
	double x, y, yPrev;
	y = legendre(a, n);

	// ���������, �� ������� ������������ ����� ����������
	// n ������ => n ��� �����
	while (xSignChangeStore.size() != n)
	{
		for (size_t i = 0; i < nodesNumber; i++)
		{
			x = a + i * h;
			yPrev = y;
			y = legendre(x, n);

			// ��������� ��������� ����, ��� ��������� ������� ����
			if (yPrev * y < 0)
			{
				xSignChangeStore.push_back({ a + (i - 1) * h , x });
			}
		}

		nodesNumber += 100;
	}

	for (size_t i = 0; i < xSignChangeStore.size(); i++)
	{
		roots.push_back(fork(xSignChangeStore[i][0], xSignChangeStore[i][1], eps, n));
	}

	return roots;
}

double quadGaussLegendre(double a, double b, size_t N)
{
	double S;
	
	vector<double> nodes = findLegendreRoots(0.000000001, N);

	// ���������� ������� ������������ �����
	vector<vector<double>> A(N);
	for (size_t i = 0; i < N; i++)
	{
		A[i].resize(N);
	}
	vector<double> B(N);

	for (size_t q = 0; q < N; q++)
	{
		// ���������� �������
		for (size_t j = 0; j < N; j++)
		{
			A[q][j] = pow(nodes[j], q);
		}

		// ���������� ������� ��������� ������
		if (q % 2 == 0) B[q] = 2.0 / (q + 1ul);
		else B[q] = 0;
	}

	// ������ �������
	vector<double> weights = solveGauss(A, B);

	// ��������� ����� � ������ ������ ����������
	S = computeSum(nodes, weights, a, b);

	return S;
}

double quadOptimalNodes(Grid& grid, size_t q)
{
	// ������� [a,b] ����������� �� q ������ �������� b[l]
	vector<double> b(q);
	for (size_t i = 0; i < q; i++)
	{
		b[i] = (grid.b - grid.a) / q;
	}

	size_t nodesNumber = 100;
	vector<double> h(q);
	for (size_t i = 0; i < q; i++)
	{
		h[i] = b[i] / (nodesNumber - 1);
	}

	// ������� �������� ������ ����������� �� ������ �������
	vector<double> A(q);
	// ���� �� ��������
	for (size_t i = 0; i < q; i++)
	{
		A[i] = -100000000.0;
		// ���� �� ����������� �� �������
		for (size_t j = 0; j < nodesNumber; j++)
		{
			if (abs(secondDerivative(grid.a + i * b[i] + j * h[i])) > A[i])
				A[i] = abs(secondDerivative(grid.a + i * b[i] + j * h[i]));
		}
	}

	// ��������� ������ (����������� �� ������)
	double lambda;
	double sum = 0.0;
	for (size_t l = 0; l < q; l++)
	{
		//sum += cbrt(A[l] / 48.0) * b[l];
		sum += b[l] * cbrt(A[l] / 6.0);
	}
	//lambda = pow(grid.optimalDelta / sum, 1.5);
	lambda = pow(sum / (grid.n - 1), 3);

	// ��������� ���������� ����������� �� ������ ������� b[l]
	vector<size_t> N(q);
	for (size_t l = 0; l < q; l++)
	{
		//cout << b[l] * cbrt(A[l] / 6.0 / lambda) << endl;
		N[l] = b[l] * cbrt(A[l] / 6.0 / lambda) + 0.5;
		if (N[l] == 0)
		{
			/*cout << "Number of nodes is less than 2, trapezium formula is not applicable";
			cout << endl;*/
			//return -1;
			N[l] = 1;
		}
	}

	// ��������� �������� �� ������� ��������
	// �� ������ ������� b[l]
	double J = 0.0;
	vector<Grid> grids(q);
	for (size_t i = 0; i < q; i++)
	{
		Grid& tempGrid = grids[i];
		if (i != 0)	tempGrid.a = grids[i - 1].b;
		else tempGrid.a = grid.a;
		tempGrid.b = tempGrid.a + b[i];
		tempGrid.n = N[i] + 1;
		// ������ �����
		tempGrid.x.resize(tempGrid.n);
		tempGrid.y.resize(tempGrid.n);
		for (size_t j = 0; j < tempGrid.n; j++)
		{
			tempGrid.x[j] = tempGrid.a + j * (tempGrid.b - tempGrid.a) / (tempGrid.n - 1);
			tempGrid.y[j] = func(tempGrid.x[j]);
		}

		J += trapeziumInt(tempGrid);

		tempGrid.x.clear();
		tempGrid.y.clear();
	}

	size_t totalNodes = 0;
	for (size_t i = 0; i < q; i++)
	{
		cout << "i = " << i << ", segments = " << N[i] << endl;
		totalNodes += N[i];
	}
	cout << "total nodes = " << totalNodes + 1 << endl;

	return J;
}