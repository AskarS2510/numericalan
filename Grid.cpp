#include "Grid.h"

double func(double x)
{
	return exp(-x * x);
	//return x * x * x * sqrt(1 - x * x);
}

double secondDerivative(double x)
{
	return exp(-x * x) * (4 * x * x - 2);
}
