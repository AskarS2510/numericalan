#define _USE_MATH_DEFINES
#include "tests.h"

double lagrange(Grid& grid, double xValue)
{
	double value = 0;
	double buffer;

	for (size_t i = 0; i < grid.n; i++)
	{
		buffer = 1;
		for (size_t j = 0; j < grid.n; j++)
		{
			if (i != j) {
				buffer = buffer * (xValue - grid.x[j]) / (grid.x[i] - grid.x[j]);
			}
		}
		value = value + buffer * grid.y[i];
	}

	return value;
}

int testSpline()
{
	Grid myGrid;
	size_t nodesNumber = 100000;

	vector<double> x(nodesNumber);

	vector<double> functionValues(nodesNumber);

	vector<double> splineValues(nodesNumber);

	vector<double> delta(nodesNumber);
	vector<double> absDelta(nodesNumber);

	double optimalDelta = 0.0001;
	double h = (myGrid.b - myGrid.a) / (nodesNumber - 1);
	double maxDelta = 1;
	myGrid.n = 2;
	bool output = true; // ����� �����
	bool newtonBool = true;
	size_t step = 100;

	// ���������� ���������� ����� ��� ����������� �����������
	while (maxDelta > optimalDelta)
	{
		myGrid.n++;
		myGrid.x = vector<double>(myGrid.n);
		myGrid.y = vector<double>(myGrid.n);

		for (size_t i = 0; i < myGrid.n; i++)
		{
			myGrid.x[i] = myGrid.a + i * (myGrid.b - myGrid.a) / (myGrid.n - 1);
			myGrid.y[i] = func(myGrid.x[i]);

			//cout << myGrid.x[i] << "\t";
			//cout << myGrid.y[i] << endl;
		}

		CubeSpline Spline(myGrid);

		for (size_t i = 0; i < nodesNumber; i++)
		{
			x[i] = myGrid.a + i * h;

			splineValues[i] = Spline.CubeSplineValue(x[i]);
			functionValues[i] = func(x[i]);

			delta[i] = functionValues[i] - splineValues[i];
			absDelta[i] = abs(delta[i]);
		}

		maxDelta = *max_element(absDelta.begin(), absDelta.end());

		cout << "Delta = " << setprecision(15) << maxDelta << "\t, defined nodes number = " << myGrid.n << endl;
	}

	cout << "Minimal nodes number: " << myGrid.n << ", with delta = " << maxDelta << endl;


	if (output)
	{
		ofstream fError;
		ofstream fSpline;
		ofstream fFunc;
		fError.open("data//error.txt");
		fSpline.open("data//spline.txt");
		fFunc.open("data//func.txt");

		// ����� ����� ������
		for (size_t i = 0; i < nodesNumber; i += step)
		{
			fError << x[i] << " " << delta[i] << " " << endl;
			fSpline << x[i] << " " << splineValues[i] << " " << endl;
			fFunc << x[i] << " " << functionValues[i] << " " << endl;
		}

		fError.close();
		fSpline.close();
		fFunc.close();
	}

	return 0;
}

int compareLagrangeNewton()
{
	Grid myGrid;
	size_t nodesNumber = 100000;

	vector<double> x(nodesNumber);

	vector<double> functionValues(nodesNumber);

	vector<double> newtonValues(nodesNumber);
	vector<double> lagrangeValues(nodesNumber);

	vector<double> delta(nodesNumber);
	vector<double> absDelta(nodesNumber);

	double h = (myGrid.b - myGrid.a) / (nodesNumber - 1);
	double maxDelta;
	bool output = true; // ����� �����
	bool newtonBool = true;
	size_t step = 100;

	// ��������� ����������� �������� � �������
	myGrid.n = 16;
	h = (myGrid.b - myGrid.a) / (nodesNumber - 1);
	myGrid.x = vector<double>(myGrid.n);
	myGrid.y = vector<double>(myGrid.n);

	for (size_t i = 0; i < myGrid.n; i++)
	{
		myGrid.x[i] = myGrid.a + i * (myGrid.b - myGrid.a) / (myGrid.n - 1);
		myGrid.y[i] = func(myGrid.x[i]);
	}

	NewtonPolinom Newton(myGrid);

	for (size_t i = 0; i < nodesNumber; i++)
	{
		x[i] = myGrid.a + i * h;

		lagrangeValues[i] = lagrange(myGrid, x[i]);
		newtonValues[i] = Newton.NewtonValue(x[i]);

		delta[i] = lagrangeValues[i] - newtonValues[i];
		absDelta[i] = abs(delta[i]);
	}
	maxDelta = *max_element(absDelta.begin(), absDelta.end());

	cout << "Delta = " << setprecision(15) << maxDelta << "\t, defined nodes number = " << myGrid.n << endl;

	if (output)
	{
		ofstream fError;
		ofstream fNewton;
		ofstream fLagrange;
		fError.open("data//diffLagrangeNewton.txt");
		fNewton.open("data//newton.txt");
		fLagrange.open("data//lagrange.txt");

		// ����� ����� ������
		for (size_t i = 0; i < nodesNumber; i += step)
		{
			fError << x[i] << " " << delta[i] << " " << endl;
			fNewton << x[i] << " " << newtonValues[i] << " " << endl;
			fLagrange << x[i] << " " << lagrangeValues[i] << " " << endl;
		}

		fError.close();
		fNewton.close();
		fLagrange.close();
	}

	return 0;
}

double taylor(double x)
{
	return .999982657939700 - 9.034403382 * pow(10, (-8)) * pow(x, 20) + 0.275275978803964e-5 * pow(x, 19) - 0.389917973210231e-4 * pow(x, 18) + 0.339590027712158e-3 * pow(x, 17) - 0.202829852608776e-2 * pow(x, 16) + 0.879093434642822e-2 * pow(x, 15) - 0.286570245014949e-1 * pow(x, 14) + 0.724655795338971e-1 * pow(x, 13) - .147284366107977 * pow(x, 12) + .249332785913573 * pow(x, 11) - .352840597119407 * pow(x, 10) + .393340615821138 * pow(x, 9) - .328949121015887 * pow(x, 8) + .286604522014961 * pow(x, 7) - .346732052722810 * pow(x, 6) + 0.904417498008345e-1 * pow(x, 5) + .464564487736686 * pow(x, 4) + 0.104267349858464e-1 * pow(x, 3) - 1.00216467006408 * pow(x, 2) + 0.282238680659876e-3 * x;
}

int testTaylor()
{
	Grid myGrid;
	size_t nodesNumber = 100000;

	vector<double> x(nodesNumber);

	vector<double> functionValues(nodesNumber);

	vector<double> taylorValues(nodesNumber);

	vector<double> delta(nodesNumber);
	vector<double> absDelta(nodesNumber);

	double h = (myGrid.b - myGrid.a) / (nodesNumber - 1);
	double maxDelta;
	bool output = true; // ����� �����
	bool newtonBool = true;
	size_t step = 100;

	for (size_t i = 0; i < nodesNumber; i++)
	{
		x[i] = myGrid.a + i * h;

		taylorValues[i] = taylor(x[i]);
		functionValues[i] = func(x[i]);

		delta[i] = functionValues[i] - taylorValues[i];
		absDelta[i] = abs(delta[i]);
	}
	maxDelta = *max_element(absDelta.begin(), absDelta.end());

	cout << "Delta = " << setprecision(15) << maxDelta << endl;

	if (output)
	{
		ofstream fError;
		ofstream fTaylor;
		ofstream fFunc;
		fError.open("data//diffTaylorFunc.txt");
		fTaylor.open("data//taylor.txt");
		fFunc.open("data//func.txt");

		// ����� ����� ������
		for (size_t i = 0; i < nodesNumber; i += step)
		{
			fError << x[i] << " " << delta[i] << " " << endl;
			fTaylor << x[i] << " " << taylorValues[i] << " " << endl;
			fFunc << x[i] << " " << functionValues[i] << " " << endl;
		}

		fError.close();
		fTaylor.close();
		fFunc.close();
	}

	return 0;
}

double Lagr(int n, vector<vector<double>> points, double x) {
	double value = 0;
	double buffer;

	for (int i = 0; i < n; i++)
	{
		buffer = 1;
		for (int j = 0; j < n; j++)
		{
			if (i != j) {
				buffer = buffer * (x - points[j][0]) / (points[i][0] - points[j][0]);
			}
		}
		value = value + buffer * points[i][1];
	}

	return value;
}

int testMNRP()
{
	//������ 5
	{
		cout << fixed;
		cout.precision(15);
		double pi = 3.1415926535;
		int n = 20;
		int N = 100;
		double a = 0;
		double b = 3;
		double SupDelta = 0;//������������ ����������� �� ������� 
		double h = (b - a) / (double)(N - 1);//��� ����� ��� �����������
		vector<vector<vector<double>>> Pn(n);//������ ���� �����������
		vector<vector<double>> ChebyshZeros(n);//������ ��� ����� ���������� ���. � �������� � ���� ����� (�����)
		vector<vector<double>> BufChebyshZeros(n);//������ ��� ����� ���������� ���. � �������� � ���� ����� (�����)
		vector<vector<double>> F(N);//�������� �������� �������
		vector<double> Delta(N);//����������� � ������ �����


		//����� ��� ���� ��� ���� ������� �������� ������� 
		for (int i = 0; i < n; i++)
		{
			//���� ���������� ���.
			ChebyshZeros[i].resize(2);
			ChebyshZeros[i][0] = (a + b) / 2 + (b - a) * cos(pi * (2 * i + 1) / (2 * n)) / 2;
			ChebyshZeros[i][1] = taylor(ChebyshZeros[i][0]);
		}

		for (int i = 0; i < N; i++)
		{
			//���������� ������� ����
			Pn[n - 1].resize(N);
			Pn[n - 1][i].resize(2);
			Pn[n - 1][i][0] = a + i * h;
			Pn[n - 1][i][1] = Lagr(n, ChebyshZeros, Pn[n - 1][i][0]);//���������� ���� ������� n-1 ��� ���������� ������� n (��� �������)

			//�������� �������� �������� ������� 
			F[i].resize(2);
			F[i][0] = a + i * h;
			F[i][1] = func(F[i][0]);

			//������� ����������� 
			Delta[i] = abs(F[i][1] - Pn[n - 1][i][1]);
			SupDelta = *max_element(Delta.begin(), Delta.end());
		}



		n = n - 1;//������ ��� ��������� ���� ��� ���������� � ��� ������� 
		//���� ��� ���������� �����������
		while (SupDelta < 0.0001) {
			BufChebyshZeros = ChebyshZeros;//��������� ���������� ����� ��� ���������� �������� ������� � ����� �����
			//����� ��� ������ ����
			for (int i = 0; i < n; i++)
			{
				ChebyshZeros[i][0] = (a + b) / 2 + (b - a) * cos(pi * (2 * i + 1) / (2 * n)) / 2;
				ChebyshZeros[i][1] = Lagr(n + 1, BufChebyshZeros, ChebyshZeros[i][0]);
			}

			for (int i = 0; i < N; i++)
			{
				//���������� ����
				Pn[n - 1].resize(N);
				Pn[n - 1][i].resize(2);
				Pn[n - 1][i][0] = a + i * h;
				Pn[n - 1][i][1] = Lagr(n, ChebyshZeros, Pn[n - 1][i][0]);//���������� ���� ������� n-1 ��� ���������� ������� n

				//������� ����������� 
				Delta[i] = abs(F[i][1] - Pn[n - 1][i][1]);
				SupDelta = *max_element(Delta.begin(), Delta.end());
			}

			cout << n << ") " << SupDelta << endl;
			n = n - 1;
		}

		/*cout << "[";
		for (int i = 0; i < N; i++)
		{
			if (i != N - 1) {
				cout << "[" << F[i][0] << ", " << Pn[10][i][1] - F[i][1] << "], ";
			}
			else {
				cout << "[" << F[i][0] << ", " << Pn[10][i][1] - F[i][1] << "]]";
			}
		}*/

	}

	return 0;
}

int testSimpsonInt()
{
	Grid myGrid;
	double optimalDelta = myGrid.optimalDelta;

	vector<double> delta;

	bool output = true; // ����� �����

	double simpsonIntegralVal;
	double currentDelta = 1.0;
	double trueVal = myGrid.trueVal;

	// ��������� ���������� �����
	size_t startN = myGrid.n = 3;

	while (currentDelta > optimalDelta)
	{
		// ������ �����
		myGrid.x.resize(myGrid.n);
		myGrid.y.resize(myGrid.n);
		for (size_t i = 0; i < myGrid.n; i++)
		{
			myGrid.x[i] = myGrid.a + i * (myGrid.b - myGrid.a) / (myGrid.n - 1);
			myGrid.y[i] = func(myGrid.x[i]);
		}

		simpsonIntegralVal = simpsonInt(myGrid);

		currentDelta = abs(trueVal - simpsonIntegralVal);

		delta.push_back(currentDelta);

		cout << "Simpson delta = " << currentDelta <<
			"\tnumber of nodes = " << myGrid.n << endl;

		myGrid.n += 2;
	}
	// ����� �� while
	myGrid.n -= 2;


	if (output)
	{
		ofstream fError;
		fError.open("data//deltaSimpson.txt");

		myGrid.n = startN;

		// ����� ����� ������
		for (size_t i = 0; i < delta.size(); i++)
		{
			fError << myGrid.n << " " << delta[i] << " " << endl;
			myGrid.n += 2;
		}

		fError.close();
	}

	return 0;
}

int testRectangleInt()
{
	Grid myGrid;

	vector<double> delta;
	double optimalDelta = myGrid.optimalDelta;

	bool output = true; // ����� �����

	double rectangleIntegralVal;
	double currentDelta = 1.0;
	double trueVal = myGrid.trueVal;
	
	// ��������� ���������� �����
	size_t startN = myGrid.n = 1;

	while (currentDelta > optimalDelta)
	{
		rectangleIntegralVal = rectangleInt(myGrid);

		currentDelta = abs(trueVal - rectangleIntegralVal);

		delta.push_back(currentDelta);

		cout << "Rectangle delta = " << currentDelta <<
			"\tnumber of nodes = " << myGrid.n << endl;

		myGrid.n++;
	}
	// ����� �� while
	myGrid.n--;


	if (output)
	{
		ofstream fError;
		fError.open("data//deltaRectangle.txt");

		myGrid.n = startN;

		// ����� ����� ������
		for (size_t i = 0; i < delta.size(); i++)
		{
			fError << myGrid.n << " " << delta[i] << " " << endl;
			myGrid.n++;
		}

		fError.close();
	}

	return 0;
}

int testTrapeziumInt()
{
	Grid myGrid;
	double optimalDelta = myGrid.optimalDelta;

	vector<double> delta;
	vector<size_t> numberOfNodes;

	bool output = true; // ����� �����

	double trapeziumIntegralVal;
	double currentDelta = 1.0;
	double trueVal = myGrid.trueVal;

	// ��������� ���������� �����
	size_t startN = myGrid.n = 2;

	while (currentDelta > optimalDelta)
	{
		// ������ �����
		myGrid.x.resize(myGrid.n);
		myGrid.y.resize(myGrid.n);
		for (size_t i = 0; i < myGrid.n; i++)
		{
			myGrid.x[i] = myGrid.a + i * (myGrid.b - myGrid.a) / (myGrid.n - 1);
			myGrid.y[i] = func(myGrid.x[i]);
		}

		trapeziumIntegralVal = trapeziumInt(myGrid);

		currentDelta = abs(trueVal - trapeziumIntegralVal);

		delta.push_back(currentDelta);
		numberOfNodes.push_back(myGrid.n);

		cout << "Trapezium delta = " << currentDelta <<
			"\tnumber of nodes = " << myGrid.n << endl;

		myGrid.n++;
	}
	// ����� �� while
	myGrid.n--;


	outputValues("data//deltaTrapezium.txt", numberOfNodes, delta, 1);

	return 0;
}

int testGaussLegendre()
{
	Grid myGrid;

	double trueVal = myGrid.trueVal;
	double optimalDelta = myGrid.optimalDelta;
	double S;
	double currentDelta;

	vector<double> delta;

	// ������� ���������� ��������
	size_t N = 0;

	do
	{
		N++;
		S = quadGaussLegendre(myGrid.a, myGrid.b, N);

		currentDelta = abs(trueVal - S);
		delta.push_back(currentDelta);

		cout << "Gauss quad delta = " << currentDelta << "\tnodes number = " << N;
		cout << endl;
	} while (currentDelta > optimalDelta);

	ofstream fLegendre;
	
	N = 0;
	fLegendre.open("data//deltaLegendre.txt");
	for (size_t i = 0; i < delta.size(); i++)
	{
		N++;

		fLegendre << N << " " << delta[i] << " " << endl;
	}
	fLegendre.close();

	return 0;
}

int testRationalInterpolation()
{
	Grid myGrid;

	// p - ������� ���������� � ���������
	// q - ������� ���������� � �����������
	size_t p, q, n;
	p = 10; q = 15;
	n = p + q + 1;

	myGrid.n = n;

	myGrid.x = vector<double>(n);
	myGrid.y = vector<double>(n);

	for (size_t i = 0; i < n; i++)
	{
		myGrid.x[i] = myGrid.a + i * (myGrid.b - myGrid.a) / (n - 1);
		myGrid.y[i] = func(myGrid.x[i]);
	}

	// ������ ������������ ����������� �� �����
	RationalInterpolation rational(myGrid, p, q);

	size_t nodesNumber = 100000;
	vector<double> x(nodesNumber);
	vector<double> functionValues(nodesNumber);
	vector<double> rationalValues(nodesNumber);
	double h = (myGrid.b - myGrid.a) / (nodesNumber - 1);

	// ����� ����� ������������
	for (size_t i = 0; i < nodesNumber; i++)
	{
		x[i] = myGrid.a + i * h;
		rationalValues[i] = rational.RationalValue(x[i]);
		functionValues[i] = func(x[i]);
	}

	outputValues("data//rational.txt", x, rationalValues, 100);
	outputValues("data//func.txt", x, functionValues, 100);

	return 0;
}

int testOptimalNodes()
{
	Grid myGrid;
	double val;
	double delta = 1.0;

	myGrid.n = 1;
	while (delta > myGrid.optimalDelta)
	{
		myGrid.n++;

		for (size_t q = 2; q <= 5; q++)
		{
			val = quadOptimalNodes(myGrid, q);
			delta = abs(myGrid.trueVal - val);

			cout << "Quad optimal trapezium nodes delta = " << delta << endl;
			cout << endl;

			if (delta <= myGrid.optimalDelta)
				break;
		}
	}

	return 0;
}

int testLab4()
{
	SNEATS sneats;

	sneats.FixedPointIterationSystem(0.9, 0.5);
	sneats.PrintSystemRoots();

	/*sneats.Roots(0, 2, 100, "FixedPointIteration");
	sneats.PrintRoots();

	sneats.Roots(0, 2, 100, "Newton");
	sneats.PrintRoots();*/

	return 0;
}
