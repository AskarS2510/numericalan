#include "Interpolation.h"
#include "SystemSolving.h"

CubeSpline::CubeSpline(Grid& grid)
{
	this->grid = grid;

	// �������� �� 1 ������, ��� �����
	n = grid.n - 1;

	a.resize(n);
	b.resize(n);
	c.resize(n);
	d.resize(n);
	h.resize(n);

	for (size_t i = 0; i < n; i++)
	{
		h[i] = grid.x[i + 1] - grid.x[i];
	}

	FindCoeffs();
}

double CubeSpline::CubeSplineValue(double x)
{
	for (size_t i = 0; i < grid.n; i++)
	{
		if (x >= grid.x[i] && x <= grid.x[i + 1])
		{
			return a[i] + b[i] * (x - grid.x[i]) + c[i] * (x - grid.x[i]) * (x - grid.x[i])
				+ d[i] * (x - grid.x[i]) * (x - grid.x[i]) * (x - grid.x[i]);
		}
	}
	return -1;
}

void CubeSpline::FindCoeffs()
{
	TridiagMethod();

	for (size_t i = 0; i < n; i++)
	{
		a[i] = grid.y[i];
	}

	for (size_t i = 0; i < n - 1; i++)
	{
		b[i] = (grid.y[i + 1] - grid.y[i]) / h[i] - h[i] * (c[i + 1] + 2 * c[i]) / 3;
	}
	b[n - 1] = (grid.y[n] - grid.y[n - 1]) / h[n - 1] - h[n - 1] * c[n - 1] * 2 / 3;

	for (size_t i = 0; i < n - 1; i++)
	{
		d[i] = (c[i + 1] - c[i]) / (3 * h[i]);
	}
	d[n - 1] = -c[n - 1] / (3 * h[n - 1]);


}

void CubeSpline::TridiagMethod()
{
	size_t m = n - 1;

	// ����������� ������������
	vector<double> xi(m + 1);
	vector<double> eta(m + 1);

	// ������������ ���������������� �������
	vector<double> A(m);
	vector<double> B(m);
	vector<double> C(m);
	vector<double> D(m);

	// ���������� ������� ������������� ��� c_n
	B[0] = -2 * (h[0] + h[1]);
	C[0] = h[1];
	D[0] = 3 * ((grid.y[2] - grid.y[1]) / h[1] - (grid.y[1] - grid.y[0]) / h[0]);
	for (size_t i = 1; i < m - 1; i++)
	{
		A[i] = h[i];
		B[i] = -2 * (h[i] + h[i + 1]);
		C[i] = h[i + 1];
		D[i] = 3 * ((grid.y[i + 2] - grid.y[i + 1]) / h[i + 1] - (grid.y[i + 1] - grid.y[i]) / h[i]);
	}
	A[m - 1] = h[m - 1];
	B[m - 1] = -2 * (h[m - 1] + h[m]);
	D[m - 1] = 3 * ((grid.y[m - 1 + 2] - grid.y[m - 1 + 1]) / h[m - 1 + 1] - (grid.y[m - 1 + 1] - grid.y[m - 1 + 0]) / h[m - 1 + 0]);

	// ���������� ����������� �������������
	xi[0] = eta[0] = 0;
	for (size_t i = 0; i <= m - 1; i++)
	{
		xi[i + 1] = C[i] / (B[i] - A[i] * xi[i]);
		eta[i + 1] = (A[i] * eta[i] - D[i]) / (B[i] - A[i] * xi[i]);
	}

	// ���������� �������
	// m = n - 1
	c[n - 1] = eta[n - 1];
	for (size_t i = n - 2; i != 0; i--)
	{
		c[i] = xi[i] * c[i + 1] + eta[i];
	}
	c[0] = 0;

	//// ���������� �����
	//ofstream fout;
	//fout.open("check.txt");
	//fout << "A:=Matrix([";
	//for (int i = 0; i < m; i++)
	//{
	//	fout << "[";
	//	for (int j = 0; j < m; j++)
	//	{
	//		switch (i - j)
	//		{
	//		case 0:
	//			fout << -B[i];
	//			break;
	//		case 1:
	//			fout << A[i];
	//			break;
	//		case -1:
	//			fout << C[i];
	//			break;
	//		default:
	//			fout << "0";
	//			break;
	//		}
	//		if (j != m - 1)
	//		{
	//			fout << ",";
	//		}
	//	}
	//	if (i != m - 1)
	//	{
	//		fout << "],";
	//	}
	//	else
	//	{
	//		fout << "]";
	//	}
	//}
	//fout << "]);";
	//fout << "B:=Vector([";
	//for (size_t i = 1; i < n; i++)
	//{
	//	fout << c[i];
	//	if (i != n - 1)
	//	{
	//		fout << ",";
	//	}
	//}
	//fout << "]);Multiply(A,B);";
	//fout << "Dv:=Vector([";
	//for (size_t i = 0; i < m; i++)
	//{
	//	fout << D[i];
	//	if (i != m - 1)
	//	{
	//		fout << ",";
	//	}
	//}
	//fout << "]);";
	//fout.close();
}

NewtonPolinom::NewtonPolinom(Grid& grid)
{
	this->grid = grid;

	// ���������� ����������� ��������� ��� ����������
	diffs.resize(grid.n);
	CalcDiffs();
}

void NewtonPolinom::CalcDiffs()
{
	for (size_t i = 0; i < grid.n; i++)
	{
		diffs[i] = CalcSeparatedDiffs(0, i);
	}
}

double NewtonPolinom::CalcSeparatedDiffs(size_t i, size_t k)
{
	if (k - i == 0)
	{
		return grid.y[i];
	}

	return (CalcSeparatedDiffs(i, k - 1) - CalcSeparatedDiffs(i + 1, k)) / (grid.x[i] - grid.x[k]);
}

// ���������� �������� ���������� ������� � ����� "x"
double NewtonPolinom::NewtonValue(double x)
{
	// ���������� ������������ (x-x_0)(x-x_1)...(x-x_n-1)
	vector<double> x_minus_xn_multiple(grid.n);
	x_minus_xn_multiple[0] = 1.0;
	for (size_t i = 1; i < grid.n; i++)
	{
		x_minus_xn_multiple[i] = x_minus_xn_multiple[i - 1] * (x - grid.x[i - 1]);
	}

	// ������������ � ��������� ��������
	double y = 0.0;
	for (size_t i = 0; i < grid.n; i++)
	{
		y += diffs[i] * x_minus_xn_multiple[i];
	}

	return y;
}

RationalInterpolation::RationalInterpolation(Grid& grid, size_t p, size_t q)
{
	this->grid = grid;

	this->p = p;
	this->q = q;

	size_t n = grid.n;

	vector<double> B(n);
	vector<vector<double>> A(n);
	for (size_t i = 0; i < n; i++)
	{
		// ������������� n + 1 = p + q + 2
		A[i].resize(n + 1);
	}
	
	// ������ n ��������� ��� (n + 1) ����������
	// ��������� ��������� �� �����, ����� ������� ���������� ������� ����� ���� �������,
	// � ��� ���������� ������������� �������
	for (size_t i = 0; i < n; i++)
	{
		for (size_t j = 0; j <= p; j++)
		{
			A[i][j] = pow(grid.x[i], j);
		}

		for (size_t j = 0; j <= q; j++)
		{
			A[i][j + p + 1] = -grid.y[i] * pow(grid.x[i], j);
		}

		B[i] = 0;
	}

	coeffs = solveGauss(A, B);

	/* // ���������� �����
	
	myGrid.x[0] = -1;
	myGrid.x[1] = 0;
	myGrid.x[2] = 1;
	myGrid.x[3] = 2;
	myGrid.x[4] = 3;

	myGrid.y[0] = 1;
	myGrid.y[1] = 1;
	myGrid.y[2] = 1.0 / 3;
	myGrid.y[3] = 3;
	myGrid.y[4] = 1.0 / 13;
	 
	cout << "My coeffs: ";
	for (size_t i = 0; i < coeffs.size(); i++)
	{
		cout << coeffs[i] << " ";
	}
	cout << endl;

	vector<double> checkB(B.size());
	for (size_t i = 0; i < A.size(); i++)
	{
		for (size_t j = 0; j < A[0].size(); j++)
		{
			for (size_t k = 0; k < A[0].size(); k++)
			{
				checkB[i] += A[i][k] * coeffs[k];
			}
		}
	}

	cout << "My checkB: ";
	for (size_t i = 0; i < checkB.size(); i++)
	{
		cout << checkB[i] << " ";
	}
	cout << endl;

	coeffs[0] = -2;
	coeffs[1] = 1;
	coeffs[2] = -2;
	coeffs[3] = -1;
	coeffs[4] = -1;
	coeffs[5] = 1;

	for (size_t i = 0; i < A.size(); i++)
	{
		checkB[i] = 0;
		for (size_t j = 0; j < A[0].size(); j++)
		{
			for (size_t k = 0; k < A[0].size(); k++)
			{
				checkB[i] += A[i][k] * coeffs[k];
			}
		}
	}

	cout << "Their checkB: ";
	for (size_t i = 0; i < checkB.size(); i++)
	{
		cout << checkB[i] << " ";
	}
	cout << endl;

	cout << "Actual B: ";
	for (size_t i = 0; i < checkB.size(); i++)
	{
		cout << B[i] << " ";
	}
	cout << endl;

	cout << "Their coeffs: ";
	for (size_t i = 0; i < coeffs.size(); i++)
	{
		cout << coeffs[i] << " ";
	}
	cout << endl;*/
}

double RationalInterpolation::RationalValue(double x)
{
	double numeratorVal = 0.0;
	double denominatorVal = 0.0;
	
	for (size_t i = 0; i <= p; i++)
	{
		numeratorVal += coeffs[i] * pow(x, i);
	}

	for (size_t i = 0; i <= q; i++)
	{
		denominatorVal += coeffs[i + p + 1] * pow(x, i);
	}

	return numeratorVal / denominatorVal;
}