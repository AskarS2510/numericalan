#include "SystemSolving.h"

vector<double> solveGauss(vector<vector<double>> A, vector<double> B)
{
	size_t rows = A.size();
	size_t cols = A[0].size();

	double temp;

	// �� ������ ������ � ������� �������� ��������:
	// ������������ ����� ���, ����� �� ��������� ��� ���������� �������
	// � ��������� ��������� ���� ������� ���������
	for (size_t k = 0; k < rows; k++) // ������� �������
	{
		// ����� ������ � ������������ ��������� � ������� �������
		size_t p = k;

		// ���������� ������ ������� ��������� �������
		temp = A[k][k];

		// ����� ������������� �� ������ ��������
		for (size_t i = k + 1; i < rows; i++)
		{
			if (abs(A[i][k]) > abs(temp))
			{
				temp = A[i][k];
				p = i;
			}
		}

		// ���� ���� ������� ������ � ������� ���������, �� ������������ � �� k-�� �����
		if (p != k)
		{
			for (size_t i = k; i < cols; i++)
			{
				temp = A[k][i];
				A[k][i] = A[p][i];
				A[p][i] = temp;
			}

			temp = B[k];
			B[k] = B[p];
			B[p] = temp;
		}

		// �������� ��� ���� k-���
		for (size_t i = k + 1; i < rows; i++)
		{
			temp = A[i][k] / A[k][k];

			// �������� i-�� ������
			for (size_t j = k; j < cols; j++)
			{
				A[i][j] -= A[k][j] * temp;
			}

			B[i] -= B[k] * temp;
		}
	}

	vector<double> solution(cols);

	// ������ ��������� ���������� ���������� ��������
	for (size_t i = rows; i < cols; i++)
	{
		solution[i] = 1;
	}

	// ������� x[k] ������� ����������� ����� ����� �� �������
	for (size_t k = rows; k-- > 0; )
	{
		// ���� ����������� ��� "����" = 0, �� ����� ����� ����� ��������,
		// �.�. �� ���� ������ �� �������
		// ������ ������������ ������� �� ���������������
		if (A[k][k] == 0.0)
		{
			solution[k] = rand();
		}
		else
		{
			// �������� �� ������� ��������� ������ ��������� "����" ���� �������
			solution[k] = B[k];
			for (size_t l = k + 1; l < cols; l++)
			{
				solution[k] -= A[k][l] * solution[l];
			}
			solution[k] /= A[k][k];
		}		
	}

	return solution;
}