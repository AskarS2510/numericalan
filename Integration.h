#pragma once
#include "Grid.h"
#include "SystemSolving.h"

double simpsonInt(Grid& grid);

double rectangleInt(Grid& grid);

double trapeziumInt(Grid& grid);

double computeSum(vector<double>& nodes, vector<double>& weights, double a, double b);

double legendre(double x, size_t degree);

double fork(double left, double right, double eps, size_t n);

vector<double> findLegendreRoots(double eps, size_t n);

double quadGaussLegendre(double a, double b, size_t N);

double quadOptimalNodes(Grid& grid, size_t q);