#pragma once
#include <vector>
#include "Grid.h"
using namespace std;

class NewtonPolinom
{
public:
	NewtonPolinom(Grid& grid);
	double NewtonValue(double x);

private:
	Grid grid;
	vector<double> diffs;

	void CalcDiffs();
	double CalcSeparatedDiffs(size_t i, size_t k);
};

class CubeSpline
{
public:
	CubeSpline(Grid& grid);
	double CubeSplineValue(double x);

private:
	Grid grid;
	vector<double> a, b, c, d; // ������������ ��������
	vector<double> h;
	size_t n;

	void FindCoeffs();
	void TridiagMethod();
};

class RationalInterpolation
{
public:
	RationalInterpolation(Grid& grid, size_t p, size_t q);
	double RationalValue(double x);

private:
	Grid grid;
	size_t p, q; // ������� �����������
	vector<double> coeffs;
};