#pragma once
#include "Grid.h"
#include <cmath>
#include "Interpolation.h"
#include "Integration.h"
#include "SNEATS.h"
#include <algorithm>
#include <iomanip>
#include <iostream>
using namespace std;

template <typename T>
void outputValues(const char* fileName, vector<T>& x, vector<double>& values, size_t step)
{
	ofstream file;
	file.open(fileName);

	size_t nodesNumber = x.size();

	// ����� �����
	for (size_t i = 0; i < nodesNumber; i += step)
	{
		file << x[i] << " " << values[i] << " " << endl;
	}

	file.close();
}

double lagrange(Grid& grid, double xValue);

int testSpline();

int compareLagrangeNewton();

double taylor(double x);

int testTaylor();

double Lagr(int n, vector<vector<double>> points, double x);

int testMNRP();

int testSimpsonInt();

int testRectangleInt();

int testTrapeziumInt();

int testGaussLegendre();

int testRationalInterpolation();

int testOptimalNodes();

int testLab4();