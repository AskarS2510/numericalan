#pragma once
#include <vector>
#include <iostream>
#include <fstream>
using namespace std;

double func(double x);

double secondDerivative(double x);

struct Grid
{
	vector<double> x;
	vector<double> y;

	double a = 0, b = 3;
	//double a = 0.5, b = 3;

	size_t n = 0; // ���������� �����, � ������� �������� �������

	double trueVal = 0.8862073485;
	//double trueVal = 0.4249263418;
	
	double optimalDelta = 0.001;
};